1. [ Spring Boot Starter App. ](#desc)
2. [ Build. ](#build)

## 1. Spring Boot Starter App
This Project servers as a starter project, which can be used to start a new Spring boot application, with a minimal code change.
This also has MongoDB connection.

## 2. Build

This uses Gradle as the build tool.
https://gradle.org/

Clone the repo and run ```./gradlew eclipse``` if you use eclipse or sts

```./gradlew clean build``` to build the project.

```./gradlew bootRun``` to start the app from the terminal
