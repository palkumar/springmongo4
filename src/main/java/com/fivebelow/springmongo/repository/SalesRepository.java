package com.fivebelow.springmongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fivebelow.springmongo.domain.Sales;

public interface SalesRepository extends MongoRepository<Sales,String>{

}
