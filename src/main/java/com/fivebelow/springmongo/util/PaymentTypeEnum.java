package com.fivebelow.springmongo.util;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum PaymentTypeEnum {
	CREDIT,
	DEBIT,
	CASH
}
