package com.fivebelow.springmongo.service;

import java.util.List;
import java.util.Optional;

import com.fivebelow.springmongo.domain.Sales;
import com.fivebelow.springmongo.resource.response.dto.SalesDTO;

public interface SalesService {

	List<SalesDTO> getSales();

	Object postSales(List<Sales> sales);

	Optional<Sales> findSalseById(String id);

}
