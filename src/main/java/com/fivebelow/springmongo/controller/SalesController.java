package com.fivebelow.springmongo.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fivebelow.springmongo.domain.Sales;
import com.fivebelow.springmongo.service.SalesService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class SalesController {
	
	private SalesService salesService;
	
	 @GetMapping("/sales")
	    public ResponseEntity<Object> getSales() {
	        return ResponseEntity.ok(salesService.getSales());
	    }
	 
	 @PostMapping(value= "/sales")
		public ResponseEntity<Object> create(@RequestBody List<Sales> sales) {
		 	return ResponseEntity.ok(salesService.postSales(sales));
		}
	 
	 
	 
	 @GetMapping(value= "/sales/{id}")
		public ResponseEntity<Object> getById(@PathVariable String id) {
			return ResponseEntity.ok(salesService.findSalseById(id));
		}
	
	

}
