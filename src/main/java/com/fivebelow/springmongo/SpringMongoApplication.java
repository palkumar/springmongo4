package com.fivebelow.springmongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.WriteConcernResolver;

import com.mongodb.WriteConcern;

@SpringBootApplication
public class SpringMongoApplication {
	
	

	public static void main(String[] args) {
		SpringApplication.run(SpringMongoApplication.class, args);
	}

}
