package com.fivebelow.springmongo.log;

public enum ActionEnum {
    GET_DATA,
    POST_DATA,
    UPDATE_DATA,
    DELETE_DATA
}
